package Student;

public class StudentProgress {
    private String[] subjectArray;
    private int[] assessmentArray;

    public StudentProgress(String[] subjectArray, int[] assessmentArray) {
        this.subjectArray = subjectArray;
        this.assessmentArray = assessmentArray;
    }

    public String[] getSubjectArray() {
        return subjectArray;
    }

    public void setSubjectArray(String[] subjectArray) {
        this.subjectArray = subjectArray;
    }

    public int[] getAssessmentArray() {
        return assessmentArray;
    }

    public void setAssessmentArray(int[] assessmentArray) {
        this.assessmentArray = assessmentArray;
    }

}
