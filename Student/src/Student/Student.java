package Student;

public class Student {
    private String studentName;
    private StudentProgress studentProgress;

    public Student(String studentName, StudentProgress studentProgress) {
        this.studentName = studentName;
        this.studentProgress = studentProgress;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public StudentProgress getStudentProgress() {
        return studentProgress;
    }

    public void setStudentProgress(StudentProgress studentProgress) {
        this.studentProgress = studentProgress;
    }
}
