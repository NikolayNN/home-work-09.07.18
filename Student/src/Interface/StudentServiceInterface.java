package Interface;

import Student.Student;

public interface StudentServiceInterface {
    void countExcellentStudent(Student[] students);
    void countUnsatisfactoryStudent(Student[] students);

}
