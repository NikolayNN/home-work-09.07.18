package Service;

import Interface.DemoServiceInterface;

import Student.Student;
import Student.StudentProgress;

public class DemoService implements DemoServiceInterface{

    @Override
    public void demoService() {

    //инициализация массива студентов
        String[] subjectArray = {"physics", "mathematics","chemistry"};
        int[] assessmentArrayPetrov = {5,5,5};
        StudentProgress studentProgressPetrov = new StudentProgress(subjectArray, assessmentArrayPetrov);
        Student studentPetrov = new Student("Петров", studentProgressPetrov);

        int[] assessmentArraySidorov = {3, 4, 3};
        StudentProgress studentProgressSidorov = new StudentProgress(subjectArray, assessmentArraySidorov);
        Student studentSidorov = new Student("Сидоров", studentProgressSidorov);

        int[] assessmentArrayIvanov = {4, 5, 5};
        StudentProgress studentProgressIvanov = new StudentProgress(subjectArray, assessmentArrayIvanov);
        Student studentIvanov = new Student("Иванов", studentProgressIvanov);

        Student[] student = {studentPetrov, studentSidorov, studentIvanov};


        //test
        StudentService studentService = new StudentService();
        //подсчет отличников
        studentService.countExcellentStudent(student);
        //подсчет студентов с тройками
        studentService.countUnsatisfactoryStudent(student);

        //средний балл каждого студента
        MarksCalculationService calculationService = new MarksCalculationService();
        calculationService.marksCalculationService(studentPetrov);
        calculationService.marksCalculationService(studentIvanov);
        calculationService.marksCalculationService(studentSidorov);

        //средний балл по группе
        Group newGroup = new Group();
        newGroup.group(student);
    }

}
