package Service;

import Interface.StudentServiceInterface;
import Student.Student;

public class StudentService implements StudentServiceInterface {
    final static double EXCELLENT = 4.8;
    final static double UNSATISFACTORY = 3;
    private int countStudent;

    @Override
    public void countExcellentStudent(Student[] students) {
        countStudent = 0;
        MarksCalculationService calculationService = new MarksCalculationService();
        for (int i = 0; i < students.length; i++) {
            if (calculationService.marksCalculation(students[i]) > EXCELLENT)
                countStudent++;
        }
        System.out.println("Число отличников в группе: " + countStudent);
    }

    @Override
    public void countUnsatisfactoryStudent(Student[] students) {
        countStudent = 0;
        for (int i = 0; i < students.length; i++) {
            boolean marc = false;
            int arrySize = students[i].getStudentProgress().getAssessmentArray().length;
            for (int j = 0; j < arrySize; j++) {

                if (students[i].getStudentProgress().getAssessmentArray()[j] == UNSATISFACTORY)
                    marc = true;
            }
            if(marc)
                countStudent++;
        }
        System.out.println("Число студентов с отметкой неудовлетворительно в группе: " + countStudent);

    }

}
