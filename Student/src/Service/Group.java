package Service;

import Student.Student;

public class Group {
    private double averageGroup = 0;

    public void group(Student[] students) {
        MarksCalculationService groupCalculationService = new MarksCalculationService();

        for (int i = 0; i < students.length; i++) {
            averageGroup = averageGroup + groupCalculationService.marksCalculation(students[i]);
        }
       System.out.println("Средний балл группы: " + averageGroup/students.length);
    }
}
