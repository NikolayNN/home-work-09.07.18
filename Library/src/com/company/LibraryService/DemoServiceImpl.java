package com.company.LibraryService;

import com.company.Book.Book;
import com.company.Library.Library;
import com.company.Settings.Settings;

public class DemoServiceImpl implements DemoService {
    public Library library;

    public DemoServiceImpl(int size) {
        this.library = new Library(size);

    }
    //инициализация новых книг
    Book book1 = new Book("Java 8. Руководство для начинающих");
    Book book2 = new Book("OOP");
    Book book3 = new Book("Просто книжка");

    @Override
    public void demoService() {

        //создание объекта сервиса "Библиотека"
        LibraryServiceImpl libraryService = new LibraryServiceImpl();

        //инициализация полей книг с помощью утилитного класса Settings
        Settings.setBook1(book1);
        Settings.setBook2(book2);
        Settings.setBook3(book3);

        //test1 добавление книг в библиотеку library
        libraryService.addBook(library, book3);
        libraryService.addBook(library, book2);
        libraryService.addBook(library, book1);

        //test2 вывод в консоль полей полученных из библиотеки книг
        System.out.println(libraryService.getBook(library,"Просто книжка").getNumberOfPages());
        System.out.println(libraryService.getBook(library,"Java 8. Руководство для начинающих").getBody());

        //test3 удаление книги из библиотеки и попытка вывести в консоль ее поле
        libraryService.deleteBook(library, "Просто книжка");
        System.out.println(libraryService.getBook(library,"Просто книжка").getNumberOfPages());

        //test4 повторное удаление той же книги
        libraryService.deleteBook(library, "Просто книжка");

        //test5 WARNING!!! обращение к полям несуществующей в библиотеке книги
        // напрямую через объект library приведет к ошибке компиляции
        // System.out.println(library.getArrayBooks()[0].getAutor());
        // Корректное обращение к полям через сервис LibraryServiceImpl

    }
}




