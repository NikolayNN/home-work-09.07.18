package com.company.LibraryService;

import com.company.Book.Book;
import com.company.Library.Library;

public interface LibraryService {
    Book getBook(Library library, String name);
    void addBook(Library library, Book name);
    void deleteBook(Library library, String name);
}
