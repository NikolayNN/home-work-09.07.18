package com.company.LibraryService;

import com.company.Book.Book;
import com.company.Library.Library;
import com.company.Settings.Settings;

public class LibraryServiceImpl implements LibraryService {
    Book bookException = new Book("Книги по вашему запросу в библиотеке не обнаружено!");



    @Override
    public void addBook(Library library, Book name) {
        for (int i = 0; i < library.getSize(); i++) {
            if (library.getArrayBooks()[i] == null) {
                library.getArrayBooks()[i] = name;
                System.out.println("Книга \"" + name.getName() + "\" успешно зарегистрирована в библиотеке!");
                return;
            }
        }
        System.out.println("Библиотека заполнена! Удалите другую книгу, чтобы добавить новую!");
    }


    @Override
    public Book getBook(Library library, String name){
        for (int i = 0; i < library.getSize(); i++) {
            if(library.getArrayBooks()[i] != null)
                if (library.getArrayBooks()[i].getName() == name)
                    return library.getArrayBooks()[i];
        }
        Settings.setBookException(bookException);
        System.out.println("Книга \"" + name + "\" в библиотеке не найдена!");
        return bookException;
    }

    @Override
    public void deleteBook(Library library, String name) {

        for (int i = 0; i < library.getSize(); i++) {
            if (library.getArrayBooks()[i] != null)
                if (library.getArrayBooks()[i].getName() == name) {
                    library.getArrayBooks()[i] = null;
                    System.out.println("Книга \"" + name + "\" удалена из библиотеки!");
                    return;
                }
        }
        System.out.println("Попытка удалить несуществующую книгу.");
    }
}
