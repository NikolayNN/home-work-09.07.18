package com.company.Settings;

import com.company.Book.Book;

public class Settings {


    public static void setBook1(Book book1) {

        book1.setAutor("Г. Шилдт");
        book1.setNumberOfPages(712);
        book1.setBody("Герберт Шилдт, всемирно известный автор множества книг по программированию, " +
                "уже в начале книги знакомит читателей с тем, как создаются, компилируются и выполняются программы, " +
                "написанные на языке Java. Далее объясняются ключевые слова, синтаксис и языковые конструкции, " +
                "образующие ядро Java. Кроме того, в книге «Java 8. Руководство для начинающих» рассмотрены темы повышенной " +
                "сложности");
        book1.setYear(2015);
    }

    public static void setBook2(Book book2) {
    }

    public static void setBook3(Book book3) {

        book3.setAutor("Николай");
        book3.setNumberOfPages(10000);
        book3.setBody("Тут интересный текст");
        book3.setYear(2000);
    }

    public static void setBookException(Book bookException) {

        bookException.setAutor("Книги по вашему запросу в библиотеке не обнаружено!");
        bookException.setNumberOfPages(0);
        bookException.setBody("Книги по вашему запросу в библиотеке не обнаружено!");
        bookException.setYear(0);
    }
}
