package com.company.Library;

import com.company.Book.Book;

public class Library {
    private Book[] arrayBooks;
    private int size;

    public Library(int size) {
        arrayBooks = new Book[size];
        this.size=size;
    }

    public Book[] getArrayBooks() {
        return arrayBooks;
    }

    public void setArrayBooks(Book[] arrayBooks) {
        this.arrayBooks = arrayBooks;
    }

    public int getSize() {
        return size;
    }


}
